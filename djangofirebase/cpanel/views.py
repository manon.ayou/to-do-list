from django.shortcuts import render,redirect
from django.contrib import messages
import pyrebase

from .forms import TodoForm
from .models import Todo

config = {

    'apiKey': "AIzaSyBZX6gdErf1NTnpzAQCAkgm-Y4ds-l7Jfg",
    'authDomain': "todo-list-firebase-django.firebaseapp.com",
    'databaseURL': "https://todo-list-firebase-django-default-rtdb.europe-west1.firebasedatabase.app",
    'projectId': "todo-list-firebase-django",
    'storageBucket': "todo-list-firebase-django.appspot.com",
    'messagingSenderId': "1001230040613",
    'appId': "1:1001230040613:web:992f96740b2bd4b12b49cc"
}
firebase = pyrebase.initialize_app(config)

auth = firebase.auth()

def singIn(request):
    return render(request, "signIn.html")

def postsign(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = "invalid crediantials"
        return render(request,"signIn.html",{"msg":message})
    print(user)
    return render(request, "welcome.html",{"e":email})

def index(request):

    item_list = Todo.objects.order_by("-date")
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo')
    form = TodoForm()

    page = {
             "forms" : form,
             "list" : item_list,
             "title" : "TODO LIST",
           }
    return render(request, 'todo/index.html', page)

### function to remove item, it receive todo item id from url ##
def remove(request, item_id):
    item = Todo.objects.get(id=item_id)
    item.delete()
    messages.info(request, "item removed !!!")
    return redirect('todo')